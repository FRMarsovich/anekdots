package com.example.renat.anekdots;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tablayout_id);
        viewPager = findViewById(R.id.viewpager_id);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new ChapaevFragment(), getString(R.string.chap));
        viewPagerAdapter.addFragment(new SoldatsFragment(), getString(R.string.sold));
        viewPagerAdapter.addFragment(new NyamNyamClass(), getString(R.string.nyam));
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void onClickChap (View view) {
        final AlertDialog.Builder rzhakaDialog = new AlertDialog.Builder(this);
        rzhakaDialog.setMessage(R.string.axaxaxa);
        rzhakaDialog.setNegativeButton(R.string.o_o, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), R.string.what, Toast.LENGTH_LONG).show();
            }
        });

        rzhakaDialog.show();
    }

    public void onClickNyam (View view) {
        openQuitDialog();
    }

    public void onClickSold (View view) {
        Toast.makeText(getApplicationContext(), R.string.nextPlease, Toast.LENGTH_LONG).show();
    }

    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(MainActivity.this);
        quitDialog.setTitle(R.string.quitApp);

        quitDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        quitDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), R.string.tryagain, Toast.LENGTH_LONG).show();
            }
        });

        quitDialog.show();

    }
}
